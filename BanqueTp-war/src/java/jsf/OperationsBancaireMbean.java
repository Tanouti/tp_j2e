/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jsf;

import entites.OperationBancaire;
import gestionnaires.GestionnaireCompteBancaire;
import java.io.Serializable;
import java.util.List;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.faces.view.ViewScoped;

/**
 *
 * @author deptinfo
 */
@Named(value = "operationsBancaireMbean")
@ViewScoped
public class OperationsBancaireMbean implements Serializable{
    @EJB
    private GestionnaireCompteBancaire gc;
    private int id;

    /**
     * Creates a new instance of OperationsBancaireMbean
     */
    public OperationsBancaireMbean()
    {
    }
    public List<OperationBancaire> getListOperations()
    {
        return gc.getCompte(id).getOprations();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    
    
}
