/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jsf;

import entites.CompteBancaire;
import entites.OperationBancaire;
import gestionnaires.GestionnaireCompteBancaire;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

/**
 *
 * @author deptinfo
 */
@Named(value = "comptesBancaireMbean")
@ViewScoped
public class ComptesBancaireMbean implements Serializable {

    @EJB
    private GestionnaireCompteBancaire gc;
    List<CompteBancaire> list = new ArrayList();
    String nom;
    double solde;
    int idEmetteur;
    int idRecepteur;
    double montant;
    private LazyDataModel<CompteBancaire> model;

    List<OperationBancaire> listOperations = new ArrayList<>();

    public LazyDataModel<CompteBancaire> getModel() {
        return model;
    }

    /**
     * Creates a new instance of ComptesBancaireMbean
     */
    public ComptesBancaireMbean() {
        model = new LazyDataModel<CompteBancaire>() {

            @Override
            public List<CompteBancaire> load(int start, int max, String nomColumn, SortOrder sortOrder, Map filters) {
                System.out.println("Load de "+start + "a "+ max);
                return gc.getComptesWithPagination(start, max);
            }
            

        /*    @Override
            public int getRowCount() {
                return gc.count();
            }*/

            @Override
            public int getRowCount() {
               return gc.count(); //To change body of generated methods, choose Tools | Templates.
            }

        };
    }

    public String CreeCompteTest() {
        System.out.println("allo");
        gc.creerComptesTest();

        return "index?faces-redirect=true";
    }

    public List<CompteBancaire> allUsers() {
        if (list.isEmpty()) {
            refresh();
        }
        return list;
    }

    //Get Comptes en Lazy
    public void refresh() {
        list = gc.getAllComptes();
    }

    public String transferer() {
        String message = "Transfert effectué";
        gc.transferer(idEmetteur, idRecepteur, montant);
        refresh();
        /*  FacesContext context = FacesContext.getCurrentInstance();
         context.addMessage(null, new FacesMessage("Successful", "Your message: " + message));*/

        return "index?faces-redirect=true";
    }

    public void delete(int id) {
        String message = "compte Supprimer";
        gc.delete(id);
        refresh();
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage("Successful", "Your message: " + message));
    }

    public String create() {
        CompteBancaire bc = new CompteBancaire(nom, solde);
        gc.creerCompte(bc);
        refresh();
        return "index?faces-redirect=true";
    }

    public String details(int id) {
        return "details?id=" + id + "&amp;faces-redirect=true";
    }

    public String getNom() {
        return nom;
    }

    public double getSolde() {
        return solde;
    }

    public int getIdEmetteur() {
        return idEmetteur;
    }

    public int getIdRecepteur() {
        return idRecepteur;
    }

    public double getMontant() {
        return montant;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public void setSolde(double solde) {
        this.solde = solde;
    }

    public void setIdEmetteur(int idEmetteur) {
        this.idEmetteur = idEmetteur;
    }

    public void setIdRecepteur(int idRecepteur) {
        this.idRecepteur = idRecepteur;
    }

    public void setMontant(double montant) {
        this.montant = montant;
    }

}
