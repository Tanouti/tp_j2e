/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gestionnaires;

import entites.CompteBancaire;
import java.text.DecimalFormat;
import java.util.List;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author deptinfo
 */
@Stateless
@LocalBean
public class GestionnaireCompteBancaire {

    @PersistenceContext(unitName = "BanqueTp-ejbPU")
    private EntityManager em;

    public void creerCompte(CompteBancaire cb) {
        em.persist(cb);

    }

    public void creerComptesTest() {

        creerCompte(new CompteBancaire("John Lennon", 150000));
        creerCompte(new CompteBancaire("Cartney", 950000));
        creerCompte(new CompteBancaire("Ringo Starr", 20000));
        creerCompte(new CompteBancaire("Georges Harrisson", 100000));
        creerCompte(new CompteBancaire("Amine HALLILI", 150000));
        creerCompte(new CompteBancaire("Amine ELMALLEM", 150000));
        creerCompte(new CompteBancaire("Regis", 950000));
        creerCompte(new CompteBancaire("Toto toto", 20000));
        creerCompte(new CompteBancaire("Michel Buffa", 100000));
        creerCompte(new CompteBancaire("Pascal fasioni", 150000));
        creerCompte(new CompteBancaire("Sarah Misi", 950000));
        creerCompte(new CompteBancaire("Test Test", 20000));
        creerCompte(new CompteBancaire("Proprietaire DeCompte", 100000));
        for (int i = 1; i < 2000; i++) {
            double solde = 150000 * Math.random();
            DecimalFormat df = new DecimalFormat("########.00");
            String str = df.format(solde);
            solde = Double.parseDouble(str.replace(',', '.'));
            CompteBancaire c = new CompteBancaire("persoone" + i, solde);
            creerCompte(c);
        }
        System.out.print("*************** comptes de test créés ***********************");
    }

    public List<CompteBancaire> getAllComptes() {
        Query q = em.createQuery("select c from CompteBancaire c");
        return q.getResultList();
    }

    public List<CompteBancaire> getComptesWithPagination(int start, int max) {
        Query q = em.createQuery("select c from CompteBancaire c");
        q.setFirstResult(start);
        q.setMaxResults(max);
        return q.getResultList();
    }
    
    public int count()
    {
    Query q;
    q = em.createQuery("select count(c) from CompteBancaire c");
    Number nombre =(Number)q.getSingleResult();
    return nombre.intValue();
    }

    public void transferer(int idEmetteur, int idRecepteur, double montantDeTransfert) {
        try {
            CompteBancaire cp1 = em.find(CompteBancaire.class, idEmetteur);
            CompteBancaire cp2 = em.find(CompteBancaire.class, idRecepteur);

            System.out.println("________________ " + cp1.getNom());
            if (this.getAllComptes().contains(cp1) && this.getAllComptes().contains(cp2)) {
                cp1.debiter(montantDeTransfert);
                cp2.crediter(montantDeTransfert);
            } else {
                System.out.println("verifier l'id du recepteur ou l'emetteur");
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

    }

    public void delete(int id) {
        CompteBancaire cp = em.find(CompteBancaire.class, id);
        em.remove(cp);
    }

    public CompteBancaire getCompte(int id) {
        return em.find(CompteBancaire.class, id);

    }

    public void persist(Object object) {
        em.persist(object);
    }

    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")
}
