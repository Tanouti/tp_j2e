/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entites;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;

/**
 *
 * @author deptinfo
 */
@Entity
public class CompteBancaire implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
   @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    String nom;
    double solde;
    @Temporal(javax.persistence.TemporalType.DATE)
    Date dateCreation;
    @OneToMany(cascade = {(CascadeType.ALL)}, fetch = FetchType.EAGER)
    List<OperationBancaire> operations = new ArrayList<>();

    public CompteBancaire(String nom, double solde) {
        this.nom = nom;
        this.solde = solde;
        this.dateCreation = new Date();
        OperationBancaire op = new OperationBancaire("Creation de Compte", dateCreation, solde);
        operations.add(op);
    }

    public CompteBancaire() {
    }

    public void debiter(double montant) {
        if ((solde - montant) >= 0) {
            solde -= montant;
            OperationBancaire op = new OperationBancaire("Compte debiter", dateCreation, solde);
             operations.add(op);
        } else {
            System.out.println("Impossible d'effectuer cette operation : Solde insuffisant");
        }
    }

    public void crediter(double montant) {
        solde += montant;
         OperationBancaire op = new OperationBancaire("Compte Crediter", dateCreation, solde);
         operations.add(op);
    }

    public String getNom() {
        return nom;
    }

    public double getSolde() {
        return solde;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public void setSolde(int solde) {
        this.solde = solde;
    }

    public List<OperationBancaire> getOprations() {
        return operations;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) id;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CompteBancaire)) {
            return false;
        }
        CompteBancaire other = (CompteBancaire) object;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entites.CompteBancaire[ id=" + id + " ]";
    }

}
